import React from 'react';
import {ReactDOM} from 'react-dom';
import {Hjem} from "../src/Sider/Hjem";
import {shallow} from'enzyme';
import  {EventSide} from "../src/Sider/EventSide";

import './setup';
import { Component } from 'react-simplified';
import {NavLink} from "react-router-dom";
//eventId,navn, dato_fra, beskrivelse,varighet, bilde, ant_deltar,viktighet, kategoriId

const match = {params: {eventId: 1}}

const createFetch: Function = (dataToReturn: any) =>
    jest.fn().mockImplementation(() =>
        (new Promise((resolve, reject) => {
        resolve({
            ok: true,
            Id: '123',
            json: function() {
                return dataToReturn
            }
        });
})));

describe('CreateEvent component', () => {
    var list =[{eventId:1, navn: "testnavn", dato_fra:"2000-01-01",beskrivelse:"testbeskrivelse", varighet:2,bilde:"bilde.no", ant_deltar:0,viktighet:1,kategoriId:1}]
    let wrapper;

    beforeEach(()=>{
        //wrapper = shallow(<EventSide match={match}/>)
           global.fetch = createFetch(list);
           wrapper = shallow(<EventSide match={match}/>) ;
           wrapper.setState({resources: list});
    });

    it('find navlinks', () => {
        expect(wrapper.find(NavLink).length).toEqual(2)
    });


    it('find divs', () => {
          expect(wrapper.find('div').length).toEqual(7)
    });


    it('find buttons', () => {
        expect(wrapper.find('button').length).toEqual(2)
    });




});

