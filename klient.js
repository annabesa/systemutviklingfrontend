//@flow

import ReactDOM from 'react-dom';
import * as React from 'react';
import { Component } from 'react-simplified';
import { HashRouter, Route, NavLink } from 'react-router-dom';

class Hjem extends Component {
    render() {
        return <div>React example with component state</div>;
    }
}


class Menu extends Component{
    render(){
        return(
          null
        );
    }
}


const root = document.getElementById('root');
if (root)
    ReactDOM.render(
    <HashRouter>
    <div>
    <Menu />
    <Route exact path="/" component={Hjem} />
</div>
</HashRouter>,
root
);