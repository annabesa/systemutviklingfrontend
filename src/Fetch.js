// @flow
let url: string = "http://localhost:8020/eventer/";
let url2: string = 'http://localhost:8020/kategorier/';



//---------------------Gets-----------------------------------
    //Henter til den forsiden med viktighet 1
    export function getEventer():Promise<Array<Object>>{
        return (fetch(url,{
            method: 'GET',
            headers: {'Content-Type': 'application/json'}
        }))
            .then(response => response.json()).then(json=>{
                return json
            }).catch(error=>error.log(error))
    }


    //Henter de N siste eventene
export function getNSisteEventer():Promise<Array<Object>>{
        return (fetch(url+ "p",{
            method: 'GET',
            headers: {'Content-Type': 'application/json'}
        }))
            .then(response => response.json()).then(json=>{
                return json
            }).catch(error=>error.log(error))
    }

export function getOneEvent(id:number){
        return fetch(url+id,{
            method: 'GET',
            headers: {'Content-Type': 'application/json'}
        }).then(response => response.json()).then(json=>{
            return json
        }).catch(error=>error.log(error))
    }

export function getEventMedKategori(id:number):Promise<Array<Object>>{
    return fetch(url+'kategori/'+id,{
        method: 'GET',
        headers: {'Content-Type': 'application/json'}
    }).then(response => response.json()).then(json=>{
        return json
    }).catch(error=>error.log(error))
}



export function getKategorier():Promise<Array<Object>>{
        return (fetch(url2,{
            method: 'GET',
            headers: {'Content-Type': 'application/json'}
        }))
            .then(response => response.json()).then(json=>{
                return json
            }).catch(error=>error.log(error))
    }



export function getForums(id:number):Promise<Array<Object>>{
    return (fetch(url + 'forum/' + id,{
        method: 'GET',
        headers: {'Content-Type': 'application/json'}
    }))
        .then(response => response.json()).then(json=>{
            return json
        }).catch(error=>error.log(error))
}



//---------------------Post-----------------------------------
export function createEvent(eventBody:Object){
        fetch(url+'post',{
            method: 'POST',
            headers: {'Content-Type': 'application/json'},
            body :JSON.stringify(eventBody)
        })
            .then(response => response.json())
            .then(json => {
                console.log(JSON.stringify(json)+'nonono');

            }).catch(err => console.log(err))
    }

export function createForum(id:number, eventBody:Object){
    fetch(url+'post/forum/' + id,{
        method: 'POST',
        headers: {'Content-Type': 'application/json'},
        body :JSON.stringify(eventBody)
    })
        .then(response => response.json())
        .then(json => {
            console.log(JSON.stringify(json)+'nonono');

        }).catch(err => console.log(err))
}



//---------------------Patch-----------------------------------
export function changeOneEvent(id:number,eventBody:Object){
        fetch(url+ id+'/patch',{
            method: 'PATCH',
            headers: {'Content-Type': 'application/json'},
            body :JSON.stringify(eventBody)
        })
            .then(response => response.json())
            .then(json => {
                console.log(JSON.stringify(json)+'nonono');

            }).catch(err => console.log(err))
    }


export function changeDeltar(id: number, eventBody:Object){
    fetch(url+ 'setDeltar/' + id,{
        method: 'PATCH',
        headers: {'Content-Type': 'application/json'},
        body :JSON.stringify(eventBody),
    })
        .then(response => response.json())
        .then(json => {
            console.log(JSON.stringify(json)+'nonono');

        }).catch(err => console.log(err))
}


//---------------------Delete-----------------------------------
export function  deleteOneEvent(id:number){
        fetch(url + id+'/delete',{
            method: 'DELETE',
            headers: {'Content-Type': 'application/json'},
        })
            .then(response => response.json())
            .then(json => {
                console.log(JSON.stringify(json)+'nonono');

            }).catch(err => console.log(err))
    }



export function  deleteOneForum(id:number){
    fetch(url + 'delete/forum/'+ id,{
        method: 'DELETE',
        headers: {'Content-Type': 'application/json'},
    })
        .then(response => response.json())
        .then(json => {
            console.log(JSON.stringify(json)+'nonono');

        }).catch(err => console.log(err))
}