// @flow
import {getOneEvent, getKategorier, changeOneEvent} from "../Fetch";
import React, {Component} from "react";
import {NavLink} from "react-router-dom";


interface State  {
    resources: Array<Object> ,
    eventer: Array<Object>,
    eventnavn: string,
    dato: string ,
    beskrivelse: string,
    varighet: number,
    bilde: string,
    kategoriId: number,
    viktighet: number,
    isloaded: boolean,
}

interface Props{
    match : Object,

}



export class ChangeEvent extends Component<Props, State> {

    state = {
            resources: [],
            eventer: [],
            eventnavn: "",
            dato: "",
            beskrivelse: "",
            varighet:-1,
            bilde: "",
            kategoriId: -1,
            viktighet: -1,
        isloaded : false,
        }


    handleChange = (event : SyntheticEvent<HTMLButtonElement>)=> {
        this.setState({
            // $FlowFixMe
            kategoriId : event.target.value,
        })
    }

    handleChangeVik = (event : SyntheticEvent<HTMLButtonElement>)=> {
        this.setState({
            // $FlowFixMe
            viktighet : event.target.value,
        })
    }


    handelStringChange = (navn: string) =>(event:SyntheticEvent<HTMLInputElement>)=>{
        this.setState({
            // $FlowFixMe
            [navn]:event.target.value,
        })
    };

    getEvent = async () => {
        const id= this.props.match.params.eventId;
        await getOneEvent(id).then(e=> {
                this.setState({
                    eventer: e[0],
                    eventnavn: e[0].navn,
                    dato: e[0].dato_fra,
                    beskrivelse:e[0].beskrivelse,
                    varighet: e[0].varighet,
                    bilde: e[0].bilde,
                    kategoriId:e[0].kategoriId,
                    viktighet: e[0].viktighet

                })
        });

        await getKategorier().then((r : Array<Object>)=>{
            this.setState({
                resources: r,
                isloaded: true
            })
        })
    };

    componentDidMount() {
        this.getEvent();
    }


    change = (id : number) =>{
        let dato = this.state.dato;
        dato = dato.slice(0,10);


        let theBody: Object = {
            navn: this.state.eventnavn,
            dato_fra: dato,
            beskrivelse: this.state.beskrivelse,
            varighet: this.state.varighet,
            bilde: this.state.bilde,
            kategoriId: this.state.kategoriId,
            viktighet: this.state.viktighet
        }
        changeOneEvent(id, theBody);
    }




    render() {
        return (
            <div>
            {
                this.state.isloaded ?  <div className="container ">
                    <h1>ENDRE EVENT</h1>
                    <div className="form-group">
                        <label > Eventnavn</label>
                        <input type="text" name="Eventnavn" id="eventNavn" className="form-control" value={this.state.eventnavn} onChange={this.handelStringChange("eventnavn")} />
                    </div>

                    <div className="form-group">
                        <label >Dato</label>
                        <input type="text" name="Dato" id="dato_fra" className="form-control" value={this.state.dato.slice(0,10)} onChange={this.handelStringChange("dato")}/>
                        yyyy-mm-dd
                    </div>

                    <div className="form-group">
                        <label>Beskrivelse</label>
                        <textarea type="text" name="Beskrivelse" id="beskrivelse" className="form-control" value={this.state.beskrivelse} onChange={this.handelStringChange("beskrivelse")}/>

                    </div>

                    <div className="form-group">
                        <label >Varighet</label>
                        <input type="text" name="Varighet" id="varighet" className="form-control" value={this.state.varighet} onChange={this.handelStringChange("varighet")}/>

                    </div>

                    <div className="form-group">
                        <label>Bilde</label>
                        <input type="text" name="bilde" id="bilde" className="form-control" value={this.state.bilde} onChange={this.handelStringChange("bilde")}/>
                    </div>

                    <div className="form-group">
                        <label >Kategori</label>
                        <select name="Kategori" id="kategoriId" className="form-control" value={this.state.kategoriId} onChange={(e) => this.handleChange(e)}>

                            {
                                this.state.resources.map((r, i) => {
                                    return <option  id="kId" key={i} value={r.kategoriId}> {r.kNavn}</option>

                                })
                            }
                        </select>
                    </div>

                    <div className="form-group">
                        <label>Viktighet</label>
                        <select name="Viktighet" id="viktighet" className="form-control"  value={this.state.viktighet} onChange={(e) => this.handleChangeVik(e)}>
                            <option value={1}>1</option>
                            <option value={2}>2</option>
                        </select>
                    </div>

                    <NavLink to={"/eventer/" + this.props.match.params.eventId}>
                        <button onClick={() => this.change(this.props.match.params.eventId)}>Change event</button>
                    </NavLink>
                </div>: null
            }
            </div>
        );


    }
}