// @flow
import {getForums, deleteOneForum,createForum} from "../../Fetch";
import React, {Component} from "react";

interface State {
    resources: Array<Object>,
    brukernavn: string,
    kommentar: string,
}
interface Props {
    match:Object
}


export class Forum extends Component<Props, State>  {

   state = {
        resources: [],
        brukernavn: "",
        kommentar: ""
        }



    handleChangeB = (event: SyntheticEvent<HTMLInputElement>)=> {
        this.setState({
            // $FlowFixMe
            brukernavn : event.target.value,
        })
    }

    handleChangeK = (event: SyntheticEvent<HTMLInputElement>)=> {
        this.setState({
            // $FlowFixMe
            kommentar : event.target.value,
        })
    }


    componentDidMount() {
        const id = this.props.match.params.eventId;
        getForums(id).then(r=>{
            this.setState({
                'resources': r,
            })
        })
    }


    delete(id: number){
        deleteOneForum(id)
    }



    add= (id:number) => {
        let theBody: Object = {brukernavn: this.state.brukernavn, kommentar: this.state.kommentar}
        createForum(id, theBody)
    }


//"select navn, dato_fra, beskrivelse, varighet, bilde, ant_deltar, kNavn from eventer Natural join kategori where eventId=?
    render() {
        const resArr = this.state.resources.map((r, i) => {

            return (
                <div key={i} className="forumer">
                <div className="container">


                <div  className="card-group "  >
                    <div className="card ">


                            <p className='text-black-50'>Bruker:</p>
                            <p className="card-text" >{r.brukernavn}</p>

                            <label className='text-black-50'>Kommentar:</label>
                            <p className="card-text" >{r.kommentar}</p>
                        <button  className="float-right" onClick={()=>this.delete(r.forumId)}>Delete forum</button>
                    </div>
                </div>
                </div>
                </div>
            )
        });


        return (
            <div className="container" >
                <ul>
                    {resArr}
                </ul>

                <button className="text-center" data-toggle="collapse" href="#collapseExample"
                   aria-expanded="false" aria-controls="collapseExample">
                   Kommenter
                </button>
                <div className="collapse" id="collapseExample">
                    <div className="card card-body">

                       <label>Brukernavn</label>
                        <input type="text" name="Dato" id="brukernavn" className="form-control" onChange={this.handleChangeB}/>

                        <label>Kommentar</label>
                        <textarea type="text" name="Dato" id="kommentar" className="form-control" onChange={this.handleChangeK}/>

                        <button onClick={()=> this.add(this.props.match.params.eventId)}>Legg til</button>

                    </div>
                </div>

            </div>
        );


    }

}