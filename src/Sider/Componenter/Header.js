//flow

import {getKategorier} from "../../Fetch";
import React, {Component} from "react";
import {NavLink} from "react-router-dom";

interface State{
    resources: Array<Object>,
}
interface Props{}

export class Header extends Component<Props,State> {

   state = {
            resources: []
        }



    componentDidMount () {
        getKategorier().then(resources=>{
            this.setState({
                'resources': resources,
            })
        })
    }

    render() {
        return (
            <div className="navbar navbar-expand-sm navbar-dark bg-dark" >
                <NavLink to="/" className=" navbar-brand"> <h1>Eventer</h1></NavLink>
                <button className="navbar-toggler" data-toggle="collapse">
                    <span className="navbar-toggler-icon"></span>
                </button>

                <div className="collapse navbar-collapse" id="navbarMenu">
                    <ul className="navbar-nav ml-auto">

                        <li className="nav-item" >
                            <NavLink to="/" className="nav-link">Hjem</NavLink>
                        </li >

                        <li className="nav-item dropdown" >
                            <a  className="nav-link dropdown-toggle"  id="navbarDropdown"  data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Kategorier</a>
                            <div className="dropdown-menu" >
                                {
                                    this.state.resources.map((r,i) =>{
                                        return <NavLink to={'/eventer/kategori/'+r.kategoriId} key={i}> <div className="dropdown-item"  key ={i}> {r.kNavn} </div> </NavLink>

                                    })
                                }
                            </div>
                        </li>

                        <NavLink to="/createEvent" className="nav-item " >
                            <div className="nav-link" >Lag event</div>
                        </NavLink>
                    </ul>
                </div>
            </div>


            )
    }
}
