// @flow
import {getNSisteEventer} from "../../Fetch";
import React, {Component} from "react";
import {NavLink} from "react-router-dom";


interface State{
    resources: Array<Object>,
    isLoded: boolean
};
interface Props {}



export class LiveFeed extends Component<Props,State> {
  state = {
            resources: [],
            isLoded:false
        }



    componentDidMount () {
        getNSisteEventer().then(resources=>{
            this.setState({
                    resources: resources,
                     isLoded: true
            })
        })
    }


    render() {

        return (
            <div className="container">
            <div id="myCarousel" className="carousel slide" data-ride="carousel" data-slide-to={70}>
                <div className="carousel-inner">

                    {this.state.isLoded && (
                    <div className="carousel-item active">
                        <h1 className="display-6 text-center lead">
                           Nyeste
                            <br/>
                        </h1>
                        <h1 className="display-6 text-center lead">
                            eventer lagt til:
                        </h1>

                    </div>
                    )}
                    {this.state.resources.map((r,i)=>(

                        <div key={i} className="carousel-item">
                            <NavLink  to={'/eventer/' + r.eventId}  className={'removeBlue'}>
                        <h1  className="display-6 text-center lead" >
                            {r.navn}
                            <br/>

                        </h1>
                            <h1  className="display-6 text-center lead" >
                              {r.dato_fra.replace("T", "\t").replace(".000Z", "")}
                            </h1>
                            </NavLink>
                        </div>


                    ))

                    }
                </div>




                <a className="carousel-control-prev" href="#myCarousel" role="button" data-slide="prev">
                    <span className="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span className="sr-only">Previous</span>
                </a>
                <a className="carousel-control-next" href="#myCarousel" role="button" data-slide="next">
                    <span className="carousel-control-next-icon" aria-hidden="true"></span>
                    <span className="sr-only">Next</span>
                </a>

            </div>
            </div>

        );
    }

}