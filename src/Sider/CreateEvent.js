// @flow
import {getKategorier, createEvent} from '../Fetch';
import React ,{Component} from "react";
import {NavLink} from "react-router-dom";

interface State{
    resources: Array<Object>,
    kategoriId: number,
    kNavn: string,
    eNavn: string,
    eDato_fra:string ,
    eBeskrivelse: string,
    eVarighet: number,
    eBilde: string ,
    eVikrighet: number
};
interface Props {}


export class CreateEvent extends Component<Props,State> {
       state = {
            resources:[],
            kategoriId: -1,
            kNavn : "",
            eNavn: "",
            eDato_fra:"",
            eBeskrivelse: "",
            eVarighet: -1,
            eBilde: "",
            eVikrighet: -1
        }

    handleChangeKat = (event: SyntheticEvent<HTMLButtonElement>) : void=> {
        this.setState({
            // $FlowFixMe
            kategoriId : event.target.value,
        })
    }

    handleChangeVik = (event: SyntheticEvent<HTMLButtonElement>) : void=> {
        this.setState({
            // $FlowFixMe
            eVikrighet : event.target.value,
        })
    }

    handleChange = (statename: string) => (event: SyntheticEvent<HTMLInputElement>) : void=> {
        this.setState({
            // $FlowFixMe
            [statename] : event.target.value,
        })
    };

    componentDidMount () {
       getKategorier().then(r=>{
           this.setState({
               resources: r,
           })
       })
    }

    create = () => {

        let theBody: Object ={navn: this.state.eNavn, dato_fra: this.state.eDato_fra, beskrivelse: this.state.eBeskrivelse, varighet: this.state.eVarighet, bilde: this.state.eBilde, viktighet: this.state.eVikrighet, kategoriId: this.state.kategoriId}
        createEvent(theBody)
    };
//"select navn, dato_fra, beskrivelse, varighet, bilde, ant_deltar, kNavn from eventer Natural join kategori where eventId=?
    render() {
            return (
                <div className="container ">
                    <h1>NYTT EVENT</h1>
                    <div className="form-group">
                        <label htmlFor="eventnavn"> Eventnavn</label>
                        <input type="text" name="Eventnavn" id="eventnavn" className="form-control" onChange={this.handleChange("eNavn")}/>
                    </div>
                    <div className="form-group">
                        <label htmlFor="dato_fra">Dato</label>
                        <input type="text" name="Dato" id="dato_fra" className="form-control" onChange={this.handleChange("eDato_fra")} />
                        yyyy-mm-dd
                    </div>
                    <div className="form-group">
                        <label htmlFor="beskrivelse">Beskrivelse</label>
                        <textarea type="text" name="Beskrivelse" id="beskrivelse" className="form-control" onChange={this.handleChange("eBeskrivelse")}/>
                    </div>
                    <div className="form-group">
                        <label htmlFor="varighet">Varighet</label>
                        <input type="text" name="Varighet" id="varighet" className="form-control" onChange={this.handleChange("eVarighet")}/>
                    </div>
                    <div className="form-group">
                        <label htmlFor="bilde">Bilde</label>
                        <input type="text" name="bilde" id="bilde" className="form-control" onChange={this.handleChange("eBilde")}/>
                    </div>
                    <div className="form-group">
                        <label htmlFor="kategoriId">Kategori</label>
                        <select name="Kategori" id="katId" className="form-control" onChange={this.handleChangeKat}>
                            {
                                this.state.resources.map((r,i) => {
                                    return <option key={i} value={r.kategoriId}> {r.kNavn}</option>
                                })
                            }
                         </select>
                    </div>
                    <div className="form-group">
                        <label htmlFor="viktighet">Viktighet</label>
                        <select name="Kategori" id="viktighet" className="form-control" onChange={this.handleChangeVik}>
                            <option>1</option>
                            <option>2</option>
                        </select>
                    </div>
                    <NavLink to="/" >
                    <button onClick={this.create} >Create event</button>
                    </NavLink>
                </div>
            )
    }
}
