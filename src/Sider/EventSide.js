// @flow
import React, {Component} from "react";
import {NavLink} from "react-router-dom";
import {getOneEvent,deleteOneEvent, changeDeltar} from "../Fetch";



type State={
    resources: Array<Object>,
}
type Props ={
    match:Object
}



export class EventSide extends Component<Props,State> {
    state = {
            'resources': []
        };



    componentDidMount() {
        const id= this.props.match.params.eventId
        getOneEvent(id).then(resources=>{
            this.setState({
                'resources': resources
            })
        })
    }

    delete(eID:number) {
        const id= this.state.resources[0].eventId
        deleteOneEvent(id)
    }


    deltar(id :number, antDel: number){
        let antDeltar:number = antDel+1;
        let theBody: Object = {ant_deltar: antDeltar};
        changeDeltar(id, theBody)
    }


//"select navn, dato_fra, beskrivelse, varighet, bilde, ant_deltar, kNavn from eventer Natural join kategori where eventId=?
    render() {
        const resArr = this.state.resources.map((r, i) => {

            return (

                <div className="container" key={i}>

                    <div className="mt-5 col-md-12">

                        <div className="text-center">
                            <img src={r.bilde} width="60%" height="60%" alt={"eventbilde"}/>

                            <h1>{r.navn}  <div className="btn btn-outline-success" onClick={()=> this.deltar(r.eventId,r.ant_deltar)}>Delta</div> </h1>

                            <h5>{r.ant_deltar} skal dit</h5>

                            <p className="Date"> Dato: {(r.dato_fra).replace("T", "\t").replace(".000Z", "")}</p>
                            <p> Varighet: {r.varighet} timer</p>

                            <div className="card" width = "60%">
                                <p>{r.beskrivelse}</p>
                            </div>




                            <div>
                                <NavLink to={"/changeEvent/" + (r.eventId)} >

                                    <button type="button" className="btn btn-outline-dark" >Endre event</button>

                                </NavLink>


                                <NavLink to="/">
                                    <button  type="button" className="btn btn-outline-dark" onClick={() => this.delete(r.eventId)} >Slett event</button>
                                </NavLink>

                            </div>

                        </div>
                    </div>
                </div>
            )
        });


        return (
            <div>
                <ul>
                    {resArr}
                </ul>

            </div>
        );


    }

}

/**/