// @flow
import React, {Component} from "react";
import {NavLink} from "react-router-dom";
import {getEventMedKategori} from "../Fetch";


interface State{
    resources: Array<Object>,
};
interface Props {
    match:Object
}



export class EventSideKategori extends Component<Props,State> {
    state = {
            resources: []
        }

    componentDidMount() {
        const id= this.props.match.params.kategoriId
        getEventMedKategori(id).then(resources=>{
            this.setState({
                resources: resources,
            })
        })
    }
//"select navn, dato_fra, beskrivelse, varighet, bilde, ant_deltar, kNavn from eventer Natural join kategori where eventId=?
    render() {

        
        const resArr = this.state.resources.map((r, i) => {
            return (
                <div className="container" key={i}>
                    <div className="mt-5 col-md-12">
                        <div className="text-center">
                            <NavLink to={'/eventer/'+r.eventId} key={i}>
                                <h2 >{r.navn}</h2>
                                <img src={r.bilde} width="60%" alt={"Eventbilde"}/>
                            </NavLink>
                        </div>
                    </div>
                </div>
            )
        });
        return (
            <div>
                <ul>
                    {resArr}
                </ul>
            </div>
        );
    }
}
