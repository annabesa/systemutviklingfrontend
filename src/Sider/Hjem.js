// @flow

import React, {Component} from "react";
import {NavLink} from "react-router-dom";
import {LiveFeed} from "./Componenter/LiveFeed";
import {getEventer} from "../Fetch";


type State = {
    resources: Array<Object>,
};

type Props = {};


export class Hjem extends Component<Props,State> {

    state = {
            resources: []
        }

    componentDidMount (){
        getEventer().then(resources=>{
                this.setState({
                    resources: resources,
                })
            })
    }

    render() {
        const resArr = this.state.resources.map((r, i) => {
            return (

                <div className="container" key={i}>
                    <div className="mt-5 col-md-12">
                        <div className="text-center">
                            <NavLink to={'/eventer/' + r.eventId} key={i} className={'removeBlue'}>
                                <h2>{r.navn}</h2>
                                <img src={r.bilde} width="60%" height="60%" alt={"Eventbilde"}/>
                            </NavLink>
                        </div>
                    </div>
                </div>
            )
        });
        return (
            <div>
                <LiveFeed/>
                <ul>
                    {resArr}
                </ul>
            </div>
        );
    }
}
