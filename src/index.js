// @flow
/* eslint eqeqeq: "off" */

import ReactDOM from 'react-dom';
import * as React from 'react';
import { HashRouter, Route} from 'react-router-dom';
import {Hjem} from "./Sider/Hjem";
import {Header} from "./Sider/Componenter/Header";
import {EventSide} from "./Sider/EventSide";
import {EventSideKategori} from "./Sider/EventSideKategori";
import {CreateEvent} from "./Sider/CreateEvent";
import {ChangeEvent} from "./Sider/ChangeEvent";
import {Forum} from "./Sider/Componenter/Forum";
import {Footer} from "./Sider/Componenter/Footer";



const root = document.getElementById('root');
if (root)
  ReactDOM.render(
    <HashRouter>
      <div>
          <Header/>
          <Route exact path="/" component={Hjem} />
          <Route exact path="/createEvent" component={CreateEvent}/>
          <Route exact path="/eventer/:eventId" component={EventSide} />
          <Route exact path="/eventer/kategori/:kategoriId" component={EventSideKategori} />
          <Route exact path="/changeEvent/:eventId" component={ChangeEvent} />
          <Route exact path="/eventer/:eventId" component={Forum} />
          <Footer/>
      </div>
    </HashRouter>,
    root
  );

